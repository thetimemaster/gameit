var http = require('http');
var mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "rooter",
    password: "kappakappa12",
    database: "gameit"
});

con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
});

var server = http.createServer(function(req, res) {

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

    let url = require('url');
    let url_parts = url.parse(req.url, true);
    let query = url_parts.query;
    let site = url_parts.pathname;

    console.log(site);

    if(site === "/ranking")
    {

        res.writeHead(200);
        con.query("SELECT nick, points FROM users LEFT JOIN ranking ON ranking.user_id=users.user_id ORDER BY points DESC ", function (err, result, fields) {
            if (err)
            {
                res.write(err.toString());
                res.end();
            }
            else
            {
                res.write(JSON.stringify(result));
                res.end();
            }
        });
    }


    else if(site === "/questions")
    {
        res.writeHead(200);
        con.query("SELECT nick, question, answer FROM question_form INNER JOIN users ON question_form.user_id=users.user_id WHERE question_form.status='ok' ORDER BY time DESC", function (err, result, fields) {
            if (err)
            {
                res.write(err.toString());
                res.end();
            }
            else
            {
                res.write(JSON.stringify(result));
                res.end();
            }
        });
    }
    else if(site === "/mapNicks")
    {
        res.writeHead(200);

        con.query("SELECT user_id, nick FROM users", function (err, result, fields) {
            if (err)
            {
                res.write(err.toString());
                res.end();
            }
            else
            {
                result = {
                    "Nicks": result
                };
                res.write(JSON.stringify(result));
                res.end();
            }
        });
    }
    else if(site === "/verify")
    {


        res.writeHead(200);

        con.query("SELECT nick FROM sessions INNER JOIN users ON sessions.user_id=users.user_id WHERE sessions.token='"+query["token"]+"'", function (err, result, fields) {
            if (err)
            {
                res.write(err.toString());
                res.end();
            }
            else
            {
                let response;
                if(result.length === 0)
                {
                    response = {
                        "Status": 1,
                        "Error": "Wrong token"
                    }
                }
                else
                {
                    response = {
                        "Status": 0,
                        "Nick": result[0].nick
                    }
                }
                res.write(JSON.stringify(response));
                res.end();
            }
        });
    }

    else
    {
        res.writeHead(404);
        res.end();
    }
});

server.listen(8085);