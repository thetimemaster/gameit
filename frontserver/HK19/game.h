#ifndef GAME_H
#define GAME_H

#include <vector>
#include <string>
#include <curl/curl.h>

#define ERR_OK 0
#define ERR_NOCONN 1

std::string errorDescr(int err);

class Game
{
  private:
	CURL *curl;
	int game_id;

  public:
	int getGameMap(std::vector<Platform> &platforms);
	int getProjectiles(std::vector<Projectile> &projectiles);
	int getPlayers(std::vector<Player> &players);
	int getMyStats(PlayerStats &stats);

	int stop();
	int move(float x, float z);
	int jump();
	int shootGrenade(vec3 dir, float time);
	int shootBlackHole(vec3 dir, float time);
};

struct vec3
{
	float x, y, z;
};

struct Platform
{
	vec3 pos;
	float radius;
};

struct Projectile
{
	vec3 pos;
	vec3 vel;
	enum
	{
		GRENADE,
		BLACKHOLE
	} type;
};

struct Player
{
	vec3 pos;
	vec3 vel;
	int id;
	int platform_id;
};

struct PlayerStats
{
	vec3 pos;
	vec3 vel;
	int hp;
	bool onGround;
	int platform_id;
};

std::vector<Game> getMyGames(int my_id);

#endif
