#include "game.h"
#include <bits/stdc++.h>
using namespace std;

vector<Platform> map;
PlayerStats my_state;
static const int scale = 10;

float abs(float a) { return (a > 0) ? a : (-a); }
float how_far(float x1, float x2, float y1, float y2)
{
    if (x1 < x2)
        swap(x1, x2);
    if (y1 - y2)
        swap(y1, y2);
    return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2)(y1 - y2));
}

void platforms_distance()
{
}
struct Enemy
{
    vec3 pos, vel, distance;
    bool close;
    int id;
    int platform_id;
};

float dis_from_platform_center(Enemy *en)
{
    Platform plat = map[en.platform_id];
    float dis_form_plat = how_far(en.pos.x, plat.pos.x, en.pos.z, plat.pos.z);
}