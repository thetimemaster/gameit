import React from 'react';
import HomePage from './components/HomePage';
import Ranking from './components/Ranking';
import Downloads from './components/Downloads';

import { BrowserRouter as Router, Route } from 'react-router-dom';


import './styles/style.css';

function App() {
    return (
        <div className="app">
            <Router>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/ranking" component={Ranking} />
                <Route exact path="/downloads" component={Downloads}/>
            </Router>
        </div>

    );
}

export default App;
