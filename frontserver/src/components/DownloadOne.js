import React from 'react';

class DownloadOne extends React.Component{

    constructor(props)
    {
        super(props);

    }

    render()
    {
        return (
            <div className="box">
                <a href={this.props.target} download>
                    <p>{this.props.desc}</p>
                </a>
            </div>
        );
    }
}
export default DownloadOne;