import React from 'react';
import Header from './Header.js';
import DownloadOne from './DownloadOne';

class Downloads extends React.Component{

    constructor()
    {
        super();
    }

    render()
    {
        setTimeout(this.getData,1000);
        return (
            [
                <Header/>,
                <div className="main">
                    <div className="mark"/>
                    <DownloadOne target="rules.pdf" desc="Rules of the Midnight Game"/>
                    <DownloadOne target="cpp_linux.zip" desc="C++ Library (Linux)"/>
                    <DownloadOne target="cpp_win.zip" desc="C++ Library (Windows)"/>
                </div>
            ]
        );
    }
}
export default Downloads;