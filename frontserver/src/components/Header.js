import React from 'react';
import HeaderLink from './HeaderLink.js';
import Login from './Login';
import {Link} from 'react-router-dom';


class Header extends React.Component{

    state = {
        open: false
    }

    constructor()
    {
        super();
        this.swapMenu = this.swapMenu.bind(this);
    }

    swapMenu()
    {
        this.setState((prevState) => ({
            open: !prevState.open
        }));
    }


    render()
    {
        return (
            <header className="header header-main">
                <Link to='/' id="ourlogo">
                    <div id="logo-back"/>
                </Link>
                <div id="mobile-button" onClick={this.swapMenu}/>
                <div id="main-menu" className={this.state.open?"open":""}>
                    <HeaderLink where="/downloads" displayed="Downloads"/>
                    <HeaderLink where="/ranking" displayed="Ranking"/>
                    <HeaderLink where="/" displayed="Simulation"/>
                </div>
            </header>
        );
    }
}
export default Header;