import React from 'react';
import {Link} from 'react-router-dom';

class HeaderLink extends React.Component{

    constructor(props)
    {
        super(props);

    }

    render()
    {
        return (
            <Link to={this.props.where}>{this.props.displayed}</Link>
        );
    }
}
export default HeaderLink;