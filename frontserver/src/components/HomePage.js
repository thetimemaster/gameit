import React from 'react';
//import Button from 'react-native';
import Header from "./Header";
import Unity, { UnityContent } from "react-unity-webgl";

class HomePage extends React.Component{

    constructor(props)
    {
        super(props);

        this.unityContent = new UnityContent(
            "visualization/beta3/Build/beta3.json",
            "visualization/beta3/Build/UnityLoader.js"
        );

        this.ToggleFullscreen = this.ToggleFullscreen.bind(this);
    }

    ToggleFullscreen()
    {
        if(this.unityContent != null)
            this.unityContent.setFullscreen(true);
    }


    render()
    {
        return (
            [
                <Header/>,
                <div className="main">
                    <div className="mark"/>
                    <Unity className="unity" unityContent={this.unityContent} />
                    <a className="button" onClick={this.ToggleFullscreen}>Fullscreen</a>
                </div>

            ]
        );
    }
}
export default HomePage;