import React from 'react';
import {Link} from 'react-router-dom';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

class Login extends React.Component{

    state = {
        logged: false,
        nick: ""
    };

    checkLogged()
    {
        return fetch('http://192.168.1.41:8085/verify?token='+cookies.get("token"))
            .then((response) => response.json())
            .then((responseJson) => {

                if(responseJson.Status == 0) {
                    this.setState({
                        logged: true,
                        nick: responseJson.Nick
                    });
                }
            })
            .catch((error) => {
                console.error(error);
            });
    }

    constructor(props)
    {
        super(props);
        this.checkLogged();


        cookies.set('token', 'b9b6f7bb036ff9b9f70c7b4d54915e417f776326685511e08ec0a1df560b45cc');
    }



    render()
    {
        if(this.state.logged) return (
            <div>{this.state.nick}</div>
        );
        else return (
            <Link to="#login">LOG IN</Link>
        );
    }
}
export default Login;