import React from 'react';

class HeaderLink extends React.Component{

    constructor(props)
    {
        super(props);

    }

    render()
    {
        return (
            <div className="box question">
                <h1>{this.props.json.question}</h1>
                <h2>{this.props.json.answer}</h2>
            </div>
        );
    }
}
export default HeaderLink;