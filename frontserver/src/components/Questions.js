import React from 'react';
import Header from "./Header";
import Question from "./Question"

class Questions extends React.Component{

    state = {
        json: []
    }

    constructor(props)
    {
        super(props);
        this.getData();
    }

    getData() {
        return fetch('http://localhost:8085/questions')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    json: responseJson
                });
            })
            .catch((error) => {
                console.error(error);
            });

    }

    render()
    {
        return (
            [
                <Header/>,
                <div className="main">
                    {this.state.json.map(o => <Question json={o}/>)}
                </div>
            ]
        );
    }
}
export default Questions;