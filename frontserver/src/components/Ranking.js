import React from 'react';
import Header from './Header.js';

class Ranking extends React.Component{

    state = {
        json: []
    }

    constructor()
    {
        super();
        this.getData = this.getData.bind(this);

        this.getData();


    }

    getData() {
        return fetch('http://localhost:8085/ranking')
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    json: responseJson
                });
            })
            .catch((error) => {
                console.error(error);
            });

    }

    render()
    {
        setTimeout(this.getData,1000);
        return (
            [
                <Header/>,
                <div className="main">
                    <div className="mark"/>
                    <div className="box">
                        <table className="ranking">
                            <thead>
                                <tr className="rankingRow headRow">
                                    <td>PLACE</td>
                                    <td>NICK</td>
                                    <td>POINTS</td>
                                </tr>
                            </thead>
                            <tbody>
                            {this.state.json.map((row,i) =>
                                <tr key={i} className="rankingRow">
                                    <td>{(i+1)+"."}</td>
                                    <td>{row.nick}</td>
                                    <td>{row.points}</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </div>
                </div>
            ]
        );
    }
}
export default Ranking;