#ifndef COMMANDS_H
#define COMMANDS_H

#include <string>
#include <sstream>

#include "game.hpp"
#include "database.hpp"
#include "matchmaker.hpp"
#include "errors.hpp"

int commandLogin(mysqlx::Session &xsess,
		std::string nick,
		std::string pass,
		std::string address,
		std::string *resp_str) {

	PlayerInfo info = login(xsess, nick, pass, address);
	if (info.id == -1)
		return ERR_LOGIN;

	std::stringstream ss;
	ss << "\"Id\":" << info.id << ",\"Token\":\"" << info.token << "\"";
	*resp_str = ss.str();
	return 0;
}

int commandGetPlayerGames(Matchmaker *maker, int player_id, std::string *resp_str) {
    bool first = true;

	if (player_id < 0 || player_id >= maker->player_edges.size())
		return ERR_NO_SUCH_PLAYER;

	std::stringstream ss;
	ss << "\"Games\":[";
    for(int g_id : maker->player_edges[player_id]) {
	    if (!first)
		    ss << ",";
	    first = false;
	    ss << g_id;
    }
    ss << "]";

	*resp_str = ss.str();
	return 0;
}

int commandGetPlatforms(Matchmaker *maker, int game_id, std::string *resp_str) {
	Game* game = gameById(maker, game_id);
	if (!game)
		return ERR_NO_SUCH_GAME;

	bool first = true;

	std::stringstream ss;
	ss << "\"Platforms\":[";
	for (auto p : game->platforms) {
		if (!first)
			ss << ",";
		first = false;
		ss << "{";
		ss << "\"X\":" << p.pos.x << ",";
		ss << "\"Y\":" << p.pos.y << ",";
		ss << "\"Z\":" << p.pos.z << ",";
		ss << "\"Radius\":" << p.radius << "}";
	}
	ss << "]";

	*resp_str = ss.str();
	return 0;
}

int commandGetPlayers(Matchmaker *maker, int game_id, std::string *resp_str) {
	Game* game = gameById(maker, game_id);
	if (!game)
		return ERR_NO_SUCH_GAME;

	bool first = true;

	std::stringstream ss;
	ss << "\"Players\":[";
	for (auto p : game->players) {
		if (!first)
			ss << ",";
		first = false;
		ss << "{";
		ss << "\"X\":" << p.pos.x << ",";
		ss << "\"Y\":" << p.pos.y << ",";
		ss << "\"Z\":" << p.pos.z << ",";
		ss << "\"VX\":" << p.vel.x << ",";
		ss << "\"VY\":" << p.vel.y << ",";
		ss << "\"VZ\":" << p.vel.z << ",";
		ss << "\"ID\":" << p.player_id << "}";
	}
	ss << "]";

	*resp_str = ss.str();
	return 0;
}

int commandGetProjectiles(Matchmaker *maker, int game_id, std::string *resp_str) {
	Game* game = gameById(maker, game_id);
	if (!game)
		return ERR_NO_SUCH_GAME;

	bool first = true;

	std::stringstream ss;
	ss << "\"Projectiles\":[";
	for (auto p : game->projectiles) {
		if (!first)
			ss << ",";
		first = false;
		ss << "{";
		ss << "\"X\":" << p.pos.x << ",";
		ss << "\"Y\":" << p.pos.y << ",";
		ss << "\"Z\":" << p.pos.z << ",";
		ss << "\"VX\":" << p.vel.x << ",";
		ss << "\"VY\":" << p.vel.y << ",";
		ss << "\"VZ\":" << p.vel.z << ",";
		ss << "\"Type\":" << (p.type == 0 ? 0 : 1) << "}";
	}
	ss << "]";

	*resp_str = ss.str();
	return 0;
}

int commandGetStats(Matchmaker *maker, int game_id, int player_id, std::string *resp_str) {
	Game* game = gameById(maker, game_id);
	if (!game)
		return ERR_NO_SUCH_GAME;

	Player* player = playerById(game, player_id);
	if (!player)
		return ERR_NOT_IN_GAME;

	std::stringstream ss;
	ss << "\"X\":" << player->pos.x << ",";
	ss << "\"Y\":" << player->pos.y << ",";
	ss << "\"Z\":" << player->pos.z << ",";
	ss << "\"VX\":" << player->vel.x << ",";
	ss << "\"VY\":" << player->vel.y << ",";
	ss << "\"VZ\":" << player->vel.z << ",";
	ss << "\"Gcool\":" << (float) player->grenade_cooldown / TPS << ",";
	ss << "\"Hcool\":" << (float) player->blackhole_cooldown / TPS << ",";
	ss << "\"OnGround\":" << (player->platform_id != -1);

	*resp_str = ss.str();
	return 0;
}

int commandStop(Matchmaker *maker, int game_id, int player_id) {
	Game *game = gameById(maker, game_id);
	if (!game)
		return ERR_NO_SUCH_GAME;

	Player *player = playerById(game, player_id);
	if (!player)
		return ERR_NOT_IN_GAME;

	return stop(player) ? 0 : ERR_PLATFORM;
}

int commandMove(Matchmaker *maker, int game_id, int player_id, float dx, float dz) {
	if (dx == 0.0f && dz == 0.0f)
		return ERR_SYNTAX;

	Game *game = gameById(maker, game_id);
	if (!game)
		return ERR_NO_SUCH_GAME;

	Player *player = playerById(game, player_id);
	if (!player)
		return ERR_NOT_IN_GAME;

	return move(player, dx, dz) ? 0 : ERR_PLATFORM;
}

int commandJump(Matchmaker *maker, int game_id, int player_id) {
	Game *game = gameById(maker, game_id);
	if (!game)
		return ERR_NO_SUCH_GAME;

	Player *player = playerById(game, player_id);
	if (!player)
		return ERR_NOT_IN_GAME;

	return jump(player) ? 0 : ERR_PLATFORM;
}

int commandGrenade(Matchmaker *maker, int game_id, int player_id, float dx, float dy, float dz, float time) {
	if (dx == 0.0f && dy == 0.0f && dz == 0.0f)
		return ERR_SYNTAX;

	Game *game = gameById(maker, game_id);
	if (!game)
		return ERR_NO_SUCH_GAME;

	Player *player = playerById(game, player_id);
	if (!player)
		return ERR_NOT_IN_GAME;

	return shootGrenade(game, player, dx, dy, dz, time) ? 0 : ERR_COOLDOWN;
}

int commandBlackHole(Matchmaker *maker, int game_id, int player_id, float dx, float dy, float dz, float time) {
	if (dx == 0.0f && dy == 0.0f && dz == 0.0f)
		return ERR_SYNTAX;

	Game *game = gameById(maker, game_id);
	if (!game)
		return ERR_NO_SUCH_GAME;

	Player *player = playerById(game, player_id);
	if (!player)
		return ERR_NOT_IN_GAME;

	return shootBlackhole(game, player, dx, dy, dz, time) ? 0 : ERR_COOLDOWN;
}

#endif
