#include <mysqlx/xdevapi.h>

#include <algorithm>
#include <fstream>
#include <vector>
#include <map>
#include <ctime>
#include <queue>
#include <string>
#include <iostream>

#include "database.hpp"

#define _S std::to_string

static const char *HEX = "0123456789abcdef";

static std::string hash(std::string s) {
    std::string o = "";
    unsigned long long a = 0;
    for(int i = 0; i < s.length(); i++) a = a * 61 + s[i];
    for(int i = 0; i < 16; i++) {
        o += HEX[a % 16];
        a /= 16;
    }
    return o;
}

static std::string newToken() {
    std::string s;
    for(int i = 0; i < 64; i++)
	    s += HEX[rand() % 16];
    return s;
}

static void sanitize(std::string &stringValue)
{
    //credit https://stackoverflow.com/questions/34221156/c-sanitize-string-function
    // Add backslashes.
    for (auto i = stringValue.begin();;) {
        auto const pos = std::find_if(
                i, stringValue.end(),
                [](char const c) { return '\\' == c || '\'' == c || '"' == c; }
        );
        if (pos == stringValue.end()) {
            break;
        }
        i = std::next(stringValue.insert(pos, '\\'), 2);
    }

    // Removes others.
    stringValue.erase(
            std::remove_if(
                    stringValue.begin(), stringValue.end(), [](char const c) {
                        return '\n' == c || '\r' == c || '\0' == c || '\x1A' == c;
                    }
            ),
            stringValue.end()
    );
}

int idFromToken(mysqlx::Session &session, std::string token) {

    sanitize(token);

    auto req = session.sql("SELECT user_id FROM sessions WHERE token='" + token + "';")
		    .execute();
    mysqlx::Row row = req.fetchOne();
    return row ? (int)row[0] : -1;
}

void addPoints(mysqlx::Session &session, int player_id, int points) {
	auto exists = session.sql("SELECT * FROM ranking WHERE user_id = "+_S(player_id)).execute();

	mysqlx::Row row = exists.fetchOne();
	if (!row)
	    session.sql("INSERT INTO ranking VALUES ('"+_S(player_id)+"', '0')").execute();

	session.sql("UPDATE ranking SET points = (points + "+_S(points)+") WHERE user_id = "+_S(player_id)).execute();
}

PlayerInfo login(mysqlx::Session &session, std::string nick, std::string password, std::string address) {
	PlayerInfo info;
	mysqlx::Row row;

    sanitize(address);
    sanitize(nick);
    sanitize(password);

	auto user_probe = session.sql("SELECT user_id FROM users WHERE nick='" + nick + "'AND password_hash='"+hash(password)+"'").execute();

	row = user_probe.fetchOne();
	info.id = row ? (int) row[0] : -1;



	if (info.id != -1) {
		auto select = session.sql("SELECT token FROM sessions WHERE ip='"+ address +"' AND user_id='"+_S(info.id)+"'").execute();
		row = select.fetchOne();
		info.token = row ? (std::string) row[0] : newToken();

		session.sql("DELETE FROM sessions WHERE ip='" + address +"'").execute();

		session.sql("REPLACE INTO sessions VALUES ('"+_S(info.id)+"', '" + address + "', '"+info.token+"')").execute();
	}

	return info;
}
