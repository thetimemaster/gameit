#ifndef DATABASE_H
#define DATABASE_H

#include <mysqlx/xdevapi.h>
#include <string>

struct PlayerInfo {
	int id;
	std::string token;
};

int idFromToken(mysqlx::Session &xsess, std::string token);

void addPoints(mysqlx::Session &xsess, int player_id, int points);

PlayerInfo login(mysqlx::Session &xsess, std::string nick, std::string password, std::string address);

#endif
