#ifndef ERRORS_H
#define ERRORS_H

#define ERR_SYNTAX 1
#define ERR_LOGIN 2
#define ERR_NO_SUCH_GAME 3
#define ERR_NO_SUCH_PLAYER 4
#define ERR_NOT_IN_GAME 5
#define ERR_PLATFORM 6
#define ERR_COOLDOWN 7
#define ERR_TOO_MANY_REQUESTS 8
#define ERR_TOKEN 9

#endif
