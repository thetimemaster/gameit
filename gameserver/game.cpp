#include <stdio.h>
#include <vector>
#include <math.h>

#include "game.hpp"

#define X_MIN -10.0f
#define X_MAX 10.0f
#define Y_MIN 7.0f
#define Y_MAX 15.0f
#define Z_MIN -10.0f
#define Z_MAX 10.0f
#define RADIUS_MIN 2.0f
#define RADIUS_MAX 5.0f

#define GRENADE_POWER 10.0f
#define BLACKHOLE_POWER -4.0f

#define GRAVITY_ACCEL 10.0f
#define PLAYER_SPEED 2.0f
#define JUMP_SPEED 7.0f
#define GRENADE_SPEED 5.0f
#define BLACKHOLE_SPEED 3.0f
#define SHRINK_SPEED 0.5f

#define BLACKHOLE_LIFE 2.0f
#define GRENADE_COOLDOWN 5.0f
#define BLACKHOLE_COOLDOWN 15.0f

#define INIT_CHANCE 0.8f
#define CHANCE_DECREASE 0.005f
#define PLATFORM_GEN_RATIO 0.5f

#define MAX_PLATFORM_ATTEMPTS 10
#define MIN_PLATFORM_OVERLAP 2.0f
#define PLATFORMS_MIN 5
#define PLATFORMS_MAX 10

static float randf(float a, float b) {
	return a + rand() * (b - a) / RAND_MAX;
}

static float len(vec3 u) {
	return sqrt(u.x * u.x + u.y * u.y + u.z * u.z);
}

static float dst(vec3 u, vec3 v) {
	float d2 = 0;
	d2 += (u.x - v.x) * (u.x - v.x);
	d2 += (u.y - v.y) * (u.y - v.y);
	d2 += (u.z - v.z) * (u.z - v.z);
	return sqrt(d2);
}

static vec3 vecsum(vec3 u, vec3 v) {
	return vec3{u.x + v.x, u.y + v.y, u.z + v.z};
}

static vec3 vecdiff(vec3 u, vec3 v) {
	return vec3{u.x - v.x, u.y - v.y, u.z - v.z};
}

static vec3 vecscale(vec3 u, float f) {
	return vec3{u.x * f, u.y * f, u.z * f};
}


static bool intersect(Platform p1, Platform p2) {
	return dst(p1.pos, p2.pos) < p1.radius + p2.radius;
}

static Platform genPlatform() {
	Platform p;
	p.pos.x = randf(X_MIN, X_MAX);
	p.pos.y = randf(Y_MIN, Y_MAX);
	p.pos.z = randf(Z_MIN, Z_MAX);
	p.radius = randf(RADIUS_MIN, RADIUS_MAX);
	return p;
}

static void insertNewPlatform(std::vector<Platform> *platforms) {
	Platform p;
	bool ok;
	int attempts = MAX_PLATFORM_ATTEMPTS;

	do {
		attempts--;
		if (attempts < 0)
			return;
		ok = true;
		p = genPlatform();
		for (Platform q : *platforms) {
			if (q.radius > 0.0f &&
					fabs(p.pos.y - q.pos.y) < MIN_PLATFORM_OVERLAP && intersect(p, q))
				ok = false;
		}
	} while (!ok);

	// If there is an empty slot, put the new platform there
	for (int i = 0; i < (int)platforms->size(); i++)
		if ((*platforms)[i].radius == 0.0f) {
			(*platforms)[i] = p;
			return;
		}

	// If not, just append it at the end
	platforms->push_back(p);
}

static vec3 genPointOnPlatform(Platform p) {
	vec3 v;
	float r = p.radius * PLATFORM_GEN_RATIO;
	do {
		v.x = p.pos.x + randf(-r, r);
		v.y = p.pos.y;
		v.z = p.pos.z + randf(-r, r);
	} while (dst(p.pos, v) > r);

	return v;
}

void initGame(Game *game) {
	game->platforms.clear();
	game->projectiles.clear();
	game->players.clear();
	game->duration = 0;
	game->platform_chance = INIT_CHANCE;

	int platform_cnt = PLATFORMS_MIN + (rand() % (PLATFORMS_MAX - PLATFORMS_MIN));
	for (int i = 0; i < platform_cnt; i++)
		insertNewPlatform(&game->platforms);
}

void addPlayer(Game *game, int player_id) {
	int platform_id = rand() % game->platforms.size();

	Player player;
	player.pos = genPointOnPlatform(game->platforms[platform_id]);
	player.vel = vec3{0.0f, 0.0f, 0.0f};
	player.grenade_cooldown = 0;
	player.blackhole_cooldown = 0;
	player.player_id = player_id;
	player.platform_id = platform_id;

	game->players.push_back(player);
}

static void movePlayer(Player *player, std::vector<Platform> platforms) {
	if (player->platform_id >= 0) {
		Platform plt = platforms[player->platform_id];
		if (dst(plt.pos, player->pos) > plt.radius)
			player->platform_id = -1;
	}

	if (player->platform_id < 0 && player->vel.y < 0.0f) {
		int plat_id = 0;
		for (auto plat : platforms) {
			float t = (plat.pos.y - player->pos.y) / player->vel.y;
			if (0.0f < t && t < 1.0f / TPS) {
				vec3 intersect_pos = vecsum(player->pos, vecscale(player->vel, t));
				if (dst(plat.pos, intersect_pos) < plat.radius) {
					player->pos = intersect_pos;
					player->vel.y = 0.0f;
					player->platform_id = plat_id;
					return;
				}
			}
			plat_id++;
		}
	}

	if (player->platform_id >= 0) {
		if (player->vel.y <= 0.0f)
			player->vel.y = 0.0f;
		else
			player->platform_id = -1;
	}

	player->pos = vecsum(player->pos, vecscale(player->vel, 1.0f / TPS));
}

static vec3 force(vec3 player_pos, vec3 proj_pos, float power) {
	vec3 delta = vecdiff(player_pos, proj_pos);
	float l = len(delta);
	if (l == 0.0f)
		return vec3{0.0f, 0.0f, 0.0f};
	else
		return vecscale(delta, power / (l * (l * l + 1.0f)));
}

static vec3 netForce(vec3 pos, std::vector<Projectile> projectiles) {
	vec3 sum = vec3{0.0f, 0.0f, 0.0f};
	float power;

	for (auto proj : projectiles) {
		if (proj.type == GRENADE && proj.life == 0)
			power = GRENADE_POWER;
		else if (proj.type == BLACKHOLE_SUCK)
			power = BLACKHOLE_POWER;
		else break;
		sum = vecsum(sum, force(pos, proj.pos, power));
	}

	return sum;
}

void updateGame(Game *game, std::vector<int> *killed_players) {
	// Shrink platforms
	for (auto &platform : game->platforms) {
		platform.radius -= SHRINK_SPEED / TPS;
		if (platform.radius < 0.0f)
			platform.radius = 0.0f;
	}

	// Add platforms
	if (game->duration % TPS == 0) {
		if (rand() < game->platform_chance * RAND_MAX)
			insertNewPlatform(&game->platforms);
		game->platform_chance -= CHANCE_DECREASE;
	}

	// Update velocities from forces
	for (auto &player : game->players) {
		player.vel = vecsum(player.vel, netForce(player.pos, game->projectiles));
		player.vel.y -= GRAVITY_ACCEL / TPS;
	}

	// Update positions
	for (unsigned i = 0; i < game->players.size(); i++)
		movePlayer(&game->players[i], game->platforms);

	for (auto &proj : game->projectiles)
		proj.pos = vecsum(proj.pos, vecscale(proj.vel, 1.0f / TPS));

	// Update cooldowns and delete dead players
	std::vector<Player> new_players;
	for (auto player : game->players) {
		if (player.pos.y < 0.0f)
			killed_players->push_back(player.player_id);
		else {
			if (player.grenade_cooldown > 0)
				player.grenade_cooldown--;
			if (player.blackhole_cooldown > 0)
				player.blackhole_cooldown--;
			new_players.push_back(player);
		}
	}
	game->players = new_players;

	// Update projectile lives, blackhole types and delete dead projectiles
	std::vector<Projectile> new_projectiles;
	for (auto proj : game->projectiles) {
		proj.life--;
		if (proj.type == BLACKHOLE && proj.life < 0) {
			proj.type = BLACKHOLE_SUCK;
			proj.life = BLACKHOLE_LIFE * TPS;
			proj.vel = vec3{0.0f, 0.0f, 0.0f};
		}
		if (proj.life >= 0)
			new_projectiles.push_back(proj);
	}
	game->projectiles = new_projectiles;

	game->duration++;
}

Player *playerById(Game *game, int player_id) {
	for (unsigned i = 0; i < game->players.size(); i++)
		if (game->players[i].player_id == player_id)
			return &game->players[i];
	return NULL;
}

bool stop(Player *player) {
	if (player->platform_id < 0)
		return false;
	player->vel = vec3{0.0f, 0.0f, 0.0f};
	return true;
}

bool jump(Player *player) {
	if (player->platform_id < 0)
		return false;
	player->vel.y = JUMP_SPEED;
	return true;
}

bool move(Player *player, float x, float z) {
	if (player->platform_id < 0)
		return false;
	player->vel.x = x;
	player->vel.z = z;
	player->vel = vecscale(player->vel, PLAYER_SPEED / len(player->vel));
	return true;
}

bool shootGrenade(Game *game, Player *player, float x, float y, float z, float t) {
	if (player->grenade_cooldown > 0)
		return false;
	Projectile proj;
	proj.pos = player->pos;
	proj.vel = vec3{x, y, z};
	proj.vel = vecscale(proj.vel, GRENADE_SPEED / len(proj.vel));
	proj.life = t * TPS;
	proj.type = GRENADE;
	player->grenade_cooldown = GRENADE_COOLDOWN * TPS;
	game->projectiles.push_back(proj);
	return true;
}

bool shootBlackhole(Game *game, Player *player, float x, float y, float z, float t) {
	if (player->blackhole_cooldown > 0)
		return false;
	Projectile proj;
	proj.pos = player->pos;
	proj.vel = vec3{x, y, z};
	proj.vel = vecscale(proj.vel, BLACKHOLE_SPEED / len(proj.vel));
	proj.life = t * TPS;
	proj.type = BLACKHOLE;
	player->blackhole_cooldown = BLACKHOLE_COOLDOWN * TPS;
	game->projectiles.push_back(proj);
	return true;
}
