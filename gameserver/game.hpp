#ifndef GAME_H
#define GAME_H

#include <vector>

#define GRENADE 0
#define BLACKHOLE 1
#define BLACKHOLE_SUCK 2

#define TPS 60

struct vec3 {
	float x, y, z;
};

struct Platform {
	vec3 pos;
	float radius;
};

struct Projectile {
	vec3 pos, vel;
	int life, type;
};

struct Player {
	vec3 pos, vel;
	int grenade_cooldown, blackhole_cooldown;
	int player_id, platform_id;
};

struct Game {
	std::vector<Platform> platforms;
	std::vector<Projectile> projectiles;
	std::vector<Player> players;
	int duration;
	float platform_chance;
};

void initGame(Game *game);

void addPlayer(Game *game, int player_id);

Player *playerById(Game *game, int player_id);

void updateGame(Game *game, std::vector<int> *killed_players);

bool stop(Player *player);

bool jump(Player *player);

bool move(Player *player, float x, float z);

bool shootGrenade(Game *game, Player *player, float x, float y, float z, float t);

bool shootBlackhole(Game *game, Player *player, float x, float y, float z, float t);

#endif
