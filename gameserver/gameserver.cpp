#include <algorithm>
#include <vector>
#include <map>
#include <ctime>
#include <string>
#include <thread>
#include <mutex>

#include "http/server.hpp"
#include "database.hpp"
#include "commands.hpp"
#include "matchmaker.hpp"
#include "game.hpp"
#include "errors.hpp"

using namespace std;
using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

#define RESET_INTERVAL 10
#define REQUEST_LIMIT 1000000

#define DB_USER "rooter"
#define DB_PASS "kappakappa12"
#define DB_NAME "gameit"

#define GAME_BEGIN_TIME 1567620000
#define GAME_SECONDS 43200.0
#define BEGIN_MLT 1.0
#define END_MLT 12.0

std::map<std::string, int> request_cnt;
int last_reset_time;



bool allowRequest(std::string address) {


	int curr_time = time(NULL);
	if (curr_time > last_reset_time + RESET_INTERVAL) {
		request_cnt.clear();
		last_reset_time = curr_time;
	}
	request_cnt[address]++;
	return request_cnt[address] <= REQUEST_LIMIT;
}



static bool getString(SimpleWeb::CaseInsensitiveMultimap map, const char *key, std::string *val) {
	auto itr = map.find(key);
	if (itr == map.end())
		return false;
	else {
		*val = itr->second;
		return true;
	}
}

static bool getInt(SimpleWeb::CaseInsensitiveMultimap map, const char *key, int *val) {
	auto itr = map.find(key);
	if (itr == map.end())
		return false;
	else
		return sscanf(itr->second.c_str(), "%d", val) == 1;
}

static bool getFloat(SimpleWeb::CaseInsensitiveMultimap map, const char *key, float *val) {
	auto itr = map.find(key);
	if (itr == map.end())
		return false;
	else if (sscanf(itr->second.c_str(), "%f", val) != 1)
		return false;
	else if (!isfinite(*val))
		return false;
	return true;
}

static int handleRequest(shared_ptr<HttpServer::Request> request,
		Matchmaker *maker,
		mysqlx::Session &xsess,
		std::string *resp_str) {

	auto fields = request->parse_query_string();
	std::string address = request->remote_endpoint_address();

	if (!allowRequest(address))
		return ERR_TOO_MANY_REQUESTS;

	std::string command;
	if (!getString(fields, "command", &command))
		return ERR_SYNTAX;

	if (command == "login") {
		std::string nick, pass;
		if (!getString(fields, "nick", &nick) || !getString(fields, "password", &pass))
			return ERR_SYNTAX;
		else
			return commandLogin(xsess, nick, pass, address, resp_str);
	}

	if (command == "getPlayerGames") {
		int player_id;
		if (!getInt(fields, "playerId", &player_id))
			return ERR_SYNTAX;
		return commandGetPlayerGames(maker, player_id, resp_str);
	}

	int game_id;
	if (!getInt(fields, "gameId", &game_id))
		return ERR_SYNTAX;

    if (command == "getPlatforms")
	    return commandGetPlatforms(maker, game_id, resp_str);

    if (command == "getProjectiles")
		return commandGetProjectiles(maker, game_id, resp_str);

    if (command == "getPlayers")
		return commandGetPlayers(maker, game_id, resp_str);

	// The remaining commands need a token
	std::string token;
	if (!getString(fields, "token", &token))
		return ERR_SYNTAX;

    int player_id = idFromToken(xsess, token);
    if (player_id == -1)
		return ERR_TOKEN;

    if (command == "getMyStats")
        return commandGetStats(maker, game_id, player_id, resp_str);

    if (command == "stop") 
        return commandStop(maker, game_id, player_id);

    if (command == "jump")
		return commandJump(maker, game_id, player_id);

	// move just needs x and z
    float x, z;
    if (!getFloat(fields, "dx", &x) || !getFloat(fields, "dz", &z))
	    return ERR_SYNTAX;

    if (command == "move")
		return commandMove(maker, game_id, player_id, x, z);

	// And the 2 remaining ones require y and time
	float y, t;
    if (!getFloat(fields, "dy", &y) || !getFloat(fields, "time", &t))
	    return 1;

    if (command == "rocket")
		return commandGrenade(maker, game_id, player_id, x, y, z, t);

    if (command == "blackhole")
		return commandBlackHole(maker, game_id, player_id, x, y, z, t);

	// Unknown command
    return ERR_SYNTAX;
}

void errorCb(shared_ptr<HttpServer::Request> req, const SimpleWeb::error_code &code) {}



std::mutex mtx;

int main(int argc, char ** argv) {
	if (argc != 4)
		return 1;

    mysqlx::Session session("localhost", 33060, DB_USER, DB_PASS);
    session.sql("USE " DB_NAME).execute();

	Matchmaker maker;
	initMatchmaker(&maker, atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));

    auto requestCb = [&](shared_ptr<HttpServer::Response> resp, shared_ptr<HttpServer::Request> req) {
	    std::string resp_str;
		mtx.lock();
		int status = handleRequest(req, &maker, session, &resp_str);
		mtx.unlock();

		std::stringstream ss;
		ss << "{\"Status\":" << status;
		if (!resp_str.empty())
			ss << "," << resp_str;
		ss << "}";

		SimpleWeb::CaseInsensitiveMultimap headers;
		headers.emplace("content-type","application/json");
		headers.emplace("Access-Control-Allow-Origin","*");

		resp->write(SimpleWeb::StatusCode::success_ok, ss, headers);
    };

    HttpServer server;
	server.config.port = 2000;
	server.resource["^/api$"]["GET"] = requestCb;
	server.on_error = errorCb;

	// Start server in a seperate thread
	thread server_thread([&server]() { server.start(); });

	// Wait for server to start so that the client can connect
	this_thread::sleep_for(chrono::seconds(1));

	int ticks = 0;
	int last_time = 0;

	while (true) {
		int curr_time = time(NULL);
		double elapsed_seconds = difftime(curr_time, GAME_BEGIN_TIME);
		double multiplier = BEGIN_MLT + elapsed_seconds / GAME_SECONDS * (END_MLT - BEGIN_MLT);

		if (curr_time != last_time) {

			printf("time elapsed: %lf\n", elapsed_seconds);
			printf("ticks this second: %d\n", ticks);
			printf("edge shortage: %d\n", maker.edge_shortage);
			printf("multiplier: %lf\n", multiplier);

			last_time = curr_time;
			ticks = 0;
		}
		else
			ticks++;


		mtx.lock();
	    updateMatchmaker(&maker, session, multiplier);
	    mtx.unlock();

		this_thread::sleep_for(chrono::milliseconds(1000 / TPS));
	}

	// If we ever implement a "nice" way of stopping the server
	server_thread.join();
	session.close();
}

