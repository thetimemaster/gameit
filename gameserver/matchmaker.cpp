#include "game.hpp"
#include "matchmaker.hpp"
#include "database.hpp"

#define VICTORY_BONUS_RATIO 2.0f

void initMatchmaker(Matchmaker *maker, int player_cnt, int game_cnt, int edge_cnt) {
	int remaining_edges = player_cnt * game_cnt;

	if (edge_cnt < player_cnt)
		edge_cnt = player_cnt;

	if (edge_cnt < game_cnt)
		edge_cnt = game_cnt;

	if (edge_cnt > remaining_edges)
		edge_cnt = remaining_edges;

	maker->player_edges.resize(player_cnt);
	maker->games.resize(game_cnt);
	maker->last_game_id = game_cnt - 1;
	maker->edge_shortage = 0;
	maker->player_per_game_limit = 2 * edge_cnt / game_cnt;
	if (maker->player_per_game_limit > player_cnt)
		maker->player_per_game_limit = player_cnt;

	for (int i = 0; i < game_cnt; i++) {
		Game game;
		initGame(&game);

		bool added = false;
		for (int j = 0; j < player_cnt; j++) {
			// We add an edge with propability edge_cnt / remaining_edges
			double propability = (double) edge_cnt / remaining_edges;

			// TODO: limit the initial games

			// We also check if we won't run out of edges with next games
			if (rand() < RAND_MAX * propability && edge_cnt >= game_cnt - i) {
				addPlayer(&game, j);
				maker->player_edges[j].push_back(i);
				edge_cnt--;
				added = true;
			}
			remaining_edges--;
		}
		if (!added) {
			int j = rand() % player_cnt;
			addPlayer(&game, j);
			maker->player_edges[j].push_back(i);
			edge_cnt--;
		}

		maker->games[i].id = i;
		maker->games[i].game = game;
	}
}

Game *gameById(Matchmaker *maker, int game_id) {
	for (int i = 0; i < (int) maker->games.size(); i++)
		if (maker->games[i].id == game_id)
			return &maker->games[i].game;
	return NULL;
}

template <class T>
static void erase(std::vector<T> *vec, T y) {
	for (T &x : *vec)
		if (x == y) {
			std::swap(x, vec->back());
			vec->pop_back();
			return;
		}
}

void updateMatchmaker(Matchmaker *maker, mysqlx::Session &xsess, float mlt) {
	const int player_cnt = maker->player_edges.size();

	std::vector<int> killed_players;
	for (GameEntry &entry : maker->games) {
		killed_players.clear();
		updateGame(&entry.game, &killed_players);

		maker->edge_shortage += killed_players.size();
		for (auto p : killed_players) {
			erase(&maker->player_edges[p], entry.id);
			if (entry.game.players.empty())
				mlt *= VICTORY_BONUS_RATIO;
			addPoints(xsess, p, entry.game.duration * mlt);
		}

		if (entry.game.players.empty()) {
			maker->last_game_id++;
			entry.id = maker->last_game_id;
			initGame(&entry.game);

			std::vector<int> chosen_ids;
			while (chosen_ids.size() < maker->player_per_game_limit && maker->edge_shortage > 0) {
				int p_id = rand() % player_cnt;
				bool is_new = true;
				for (auto c : chosen_ids)
					if (c == p_id)
						is_new = false;
				if (is_new) {
					maker->edge_shortage--;
					addPlayer(&entry.game, p_id);
					maker->player_edges[p_id].push_back(entry.id);
					chosen_ids.push_back(p_id);
				}
			}
		}
	}
}

