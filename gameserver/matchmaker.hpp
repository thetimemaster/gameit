#ifndef MATCHMAKER_H
#define MATCHMAKER_H

#include <vector>
#include <mysqlx/xdevapi.h>

#include "game.hpp"

struct GameEntry {
	int id;
	Game game;
};

struct Matchmaker {
	int last_game_id;
	int edge_shortage;
	int player_per_game_limit;
	std::vector<std::vector<int>> player_edges;
	std::vector<GameEntry> games;
};

void initMatchmaker(Matchmaker *maker, int player_cnt, int game_cnt, int edge_cnt);

Game *gameById(Matchmaker *maker, int game_id);

void updateMatchmaker(Matchmaker *maker, mysqlx::Session &xsess, float mlt);

#endif
