#!/bin/python3
import http.client
import json

conn = http.client.HTTPConnection("10.236.255.124", 2137);

class Game:
    def __init__(self, conn, my_id):
        self.conn = conn
        self.my_id = my_id

def make_request(conn, query):
    conn.request("GET", query);

    resp = conn.getresponse();

    if resp.status != 200:
        print("dupa")
        return None

    return json.loads(resp.read());


def get_my_games(conn, my_id):
    resp = make_request(conn, "/api?command=getMyGames&playerId={}".format(my_id))
    games = []
    for game_id in resp["Games"]:
        games.append(Game(conn, game_id))
    return games

for g in get_my_games(conn, 2):
    print(g.my_id)



