#include <iostream>
#include <vector>
#include <cstdlib>

#include "game.h"
#include "vec.h"

float rand_float(float a, float b) {
	return a + rand() * (b - a) / RAND_MAX;
}

vec3 rand_vec(vec3 a, vec3 b) {
	return {
		rand_float(a.x, b.x),
		rand_float(a.y, b.y),
		rand_float(a.z, b.z),
	};
}

Platform gen_platform(vec3 v1, vec3 v2, float size_min, float size_max) {
	return {rand_vec(v1, v2), rand_float(size_min, size_max)};
}

void print_platform(Platform p) {
	std::cout << "\tnew Platform(" << 
		p.pos.x << ", " << 
		p.pos.y << ", " << 
		p.pos.z << ", " << 
		p.radius << "), " << std::endl;
}

bool intersect(Platform p, Platform q) {
	p.pos.y = q.pos.y = 0;
	return dst(p.pos, q.pos) < p.radius + q.radius;
}

int main(int argc, char **argv) {
	if (argc != 2) {
		std::cerr << "Usage: " << argv[0] << " <n>\n";
		return 1;
	}

	int n;
	sscanf(argv[1], "%i", &n);

	vec3 a = {-10, 0, -10};
	vec3 b = {10, 5, 10};

	std::vector<Platform> platforms;

	for (int i = 0; i < n; i++) {
		Platform platform;
		bool ok;
		do {
			ok = true;
			platform = gen_platform(a, b, 0, 5);

			for (auto p : platforms)
				if (intersect(p, platform))
					ok = false;
		} while (!ok);
		platforms.push_back(platform);
	}

	std::cout << "std::vector<Platform *> platforms = {" << std::endl;

	for (auto p : platforms)
		print_platform(p);

	std::cout << "};" << std::endl;

	return 0;
}

