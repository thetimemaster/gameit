﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraHandler : MonoBehaviour
{
    public float dist;

    public float size = 10, targetSize;
    public float ang;
    public GameObject camChild;

    bool IsOk(float d)
    {
        camChild.transform.localPosition = Vector3.back * d;

        foreach (Platform p in NetHandler.platforms)
        {
            if (p.radius > 0.1f)
            {
                Vector3 pp = camChild.GetComponent<Camera>().WorldToViewportPoint(p.transform.position);

                if (pp.x < 0 + p.radius / 20) return false;
                if (pp.x > 1 - p.radius / 20) return false;
                if (pp.y < 0 + p.radius / 20) return false;
                if (pp.y > 1 - p.radius / 20) return false;

            }
        }

        foreach (Player p in NetHandler.players)
        {
            Vector3 pp = camChild.GetComponent<Camera>().WorldToViewportPoint(p.transform.position);

                if (pp.x < 0.1) return false;
                if (pp.x > 0.9) return false;
                if (pp.y < 0.1) return false;
                if (pp.y > 0.9) return false;
        }

        return true;
    }

    Vector3 Center()
    {
        Player x = NetHandler.players.Find(p => p.id == NetHandler.playerId);
        Vector3 v = x?x.transform.position:Vector3.right;
        v.y = 0;
        return v;
    }

    private void Update()
    {
        transform.position = Center();

        float pp = 1;
        float kp = 100;
        while ((kp - pp) > 0.001)
        {
            float sp = (pp + kp) / 2;
            if (IsOk(sp)) kp = sp;
            else pp = sp;
        }
        targetSize = pp;
        if(size < targetSize) size +=Time.deltaTime * 10;
        if(size > targetSize) size -= Time.deltaTime * 10;
        IsOk(size);

        transform.rotation = Quaternion.LookRotation(-transform.position) * Quaternion.Euler(40, 0, 0);

        //camChild.transform.localPosition = Vector3.back * dist;
    }


}
