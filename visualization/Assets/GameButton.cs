﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameButton : MonoBehaviour
{
    public int number = 0;
    public bool hover = false;

    public void Hover(bool b) => hover = b;

    void Awake()
    {
        Update();
    }

    public void Update()
    {
        GetComponentInChildren<TMPro.TextMeshProUGUI>().text = number.ToString();
        GetComponent<Button>().interactable = NetHandler.gameId != number;
    }

    public void Click()
    {
        NetHandler.LoadGame( number);
    }
}
