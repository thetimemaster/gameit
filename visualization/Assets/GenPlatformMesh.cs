﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenPlatformMesh : MonoBehaviour
{
    const int v = 32;

    public static Mesh genMesh(float radius)
    {
        Mesh m = new Mesh();
        

        Vector3[] vert = new Vector3[v];
        Vector2[] uv = new Vector2[v];
        int[] tri = new int[(v - 2) * 3];

        for (int i = 0; i < v; i++)
        {
            vert[i] = Quaternion.Euler(0, 360 * i / v,0) * Vector3.forward * radius;
            uv[i] = new Vector2(vert[i].x, vert[i].z);
        }

        for(int i = 0;i< v-2;i++)
        {
            tri[i * 3 + 0] = 0;
            tri[i * 3 + 1] = i + 1;
            tri[i * 3 + 2] = i + 2;
        }

        m.vertices = vert;
        m.triangles = tri;
        m.uv = uv;
        m.RecalculateBounds();
        m.RecalculateNormals();
 
        
        

        return m;
    }
}
