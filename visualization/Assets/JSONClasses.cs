﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class SingleNickJSON
{
    public int user_id;
    public String nick;
}

[Serializable]
public class MapNicksJSON
{
    public SingleNickJSON[] Nicks;
}

[Serializable]
public class PlatformJSON
{
    public float X,Y,Z,Radius;
}

[Serializable]
public class PlayerJSON
{
    public float X,Y,Z,VX,VY,VZ;
    public int ID;
}

[Serializable]
public class ProjectileJSON
{
    public float X,Y,Z,VX,VY,VZ;
    public int Type;
}

[Serializable]
public class GetMapJSON
{
    public PlatformJSON[] Platforms;
    public float Time;
    public int Status;
}

[Serializable]
public class GetPlayersJSON
{
    public PlayerJSON[] Players;
	public int Status;
}

[Serializable]
public class GetProjectilesJSON
{
    public ProjectileJSON[] Projectiles;
	public int Status;
}

[Serializable]
public class GetStatsJSON
{
	public int Status;
}

[Serializable]
public class GetGamesJSON
{
	public int[] Games;
	public int Status;
}
