﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Mover : MonoBehaviour
{

    

    IEnumerator Move(float x,float z)
    {
        UnityWebRequest www = UnityWebRequest.Get(NetHandler.apiPrefix + "command=move&gameId=" + NetHandler.gameId + "&token=" + NetHandler.token+"&x="+x+"&z="+z);
        yield return www.SendWebRequest();
    }

    IEnumerator Stop()
    {
        UnityWebRequest www = UnityWebRequest.Get(NetHandler.apiPrefix + "command=move&gameId=" + NetHandler.gameId + "&token=" + NetHandler.token+"&x=0&z=0");
        yield return www.SendWebRequest();
    }

    IEnumerator Shoot()
    {
        UnityWebRequest www = UnityWebRequest.Get(NetHandler.apiPrefix + "command=blackHole&gameId=" + NetHandler.gameId + "&token=" + NetHandler.token + "&dx=0&dy=1&dz=0&time=1");//"&dx="+Player.me.dp.x+"&dy="+Player.me.dp.y+"&dz="+Player.me.dp.z+"&time=2");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else Debug.Log(www.downloadHandler.text);
    }

    IEnumerator Jump()
    {
        UnityWebRequest www = UnityWebRequest.Get(NetHandler.apiPrefix + "command=jump&gameId=" + NetHandler.gameId + "&token=" + NetHandler.token);
        yield return www.SendWebRequest();
    }

    private void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        if (Input.GetKeyDown(KeyCode.Space)) StartCoroutine(Shoot());

        Vector3 v = new Vector3(x, 0, z);

        if (x != 0 || z != 0) StartCoroutine(Move(x, z));
        else StartCoroutine(Stop());

        StartCoroutine(Jump());


    }
}
