﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;
using TMPro;

public class NetHandler : MonoBehaviour
{

    static float lastGet;

    public Transform buttonParent;
    public GameObject buttonPrefab;

    public static int req;

    public static MapNicksJSON nicks;
    public static GetMapJSON m;
    public static GetPlayersJSON p;
    public static GetProjectilesJSON o;
    public static GetGamesJSON g; 
    public static GetStatsJSON s;
    public static int todo = 0;

    public TextMeshProUGUI lvl;



    public static List<Player> players = new List<Player>();
    public static List<Platform> platforms = new List<Platform>();
    public static List<Projectile> projectiles = new List<Projectile>();

    public GameObject platfromPrefab, playerPrefab, projectilePrefab, explosion,vr;

    public static int gameId;
    //public static string apiPrefix = "http://10.236.255.124:2137/api?";
    public static string apiPrefix = "http:/192.168.1.41:100/api?";
    public static string api2Prefix = "http://192.168.1.41:8085";

    public static int playerId = 0;

    public static NetHandler I;
    public static string token = "b9b6f7bb036ff9b9f70c7b4d54915e417f776326685511e08ec0a1df560b45cc"; 

    static HashSet<int> gameNumbers = new HashSet<int>();

    public static string DeXD(string s)
    {
        s = s.Replace("( ͡° ͜ʖ ͡°)","*Lennyface*");
        s = s.Replace("Ć","C");
        return s;
    }

    IEnumerator GetPlatforms() {
        UnityWebRequest www = UnityWebRequest.Get(apiPrefix+"command=getGameMap&gameId="+gameId);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            //Debug.Log(www.downloadHandler.text.Replace("\"\"", "[]"));
            m = JsonUtility.FromJson<GetMapJSON>(www.downloadHandler.text);
			if(m.Status != 0) m = null;
        }

    }

    IEnumerator GetNickMap()
    {
        UnityWebRequest www = UnityWebRequest.Get(api2Prefix+"/mapNicks");
        yield return www.SendWebRequest();
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else 
		{
			nicks = JsonUtility.FromJson<MapNicksJSON>(www.downloadHandler.text);	

            var xd = FindObjectOfType<TMPro.TMP_Dropdown>();

            xd.ClearOptions();
            List<string> l = new List<string>();
            for(int i = 0; i < nicks.Nicks.Length; i++) l.Add(DeXD(nicks.Nicks[i].nick));
            xd.AddOptions(l);
		}
    }

    IEnumerator GetPlayers() {
        UnityWebRequest www = UnityWebRequest.Get(apiPrefix+"command=getPlayers&gameId="+gameId);
        yield return www.SendWebRequest();
        if(www.isNetworkError || www.isHttpError) {
            Debug.Log(www.error);
        }
        else 
		{
			p = JsonUtility.FromJson<GetPlayersJSON>(www.downloadHandler.text);
			if(p.Status != 0) p = null;					
		}
        
    }

    IEnumerator GetProjectiles() 
    {
        UnityWebRequest www = UnityWebRequest.Get(apiPrefix+"command=getProjectiles&gameId="+gameId);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            //Debug.Log(www.downloadHandler.text.Replace("\"\"", "[]"));
            o = JsonUtility.FromJson<GetProjectilesJSON>(www.downloadHandler.text.Replace("\"\"","[]"));
			if(o.Status != 0) o = null;
        }
    }

	IEnumerator GetFirstGameID()
    {
        UnityWebRequest www = UnityWebRequest.Get(apiPrefix+"command=getPlayerGames&playerId="+playerId);
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            g = JsonUtility.FromJson<GetGamesJSON>(www.downloadHandler.text.Replace("\"\"","[]"));
			if(g != null) 
            {
                if(!System.Array.Exists(g.Games, a => a == gameId))
                    LoadGame(System.Array.Find(g.Games, a => true));
                //else g = null;

                if(System.Array.Exists(g.Games, a => !gameNumbers.Contains(a)) ||
                    g.Games.Length != gameNumbers.Count)
                
                {
                    gameNumbers.Clear();
                    foreach(int a in g.Games) gameNumbers.Add(a);
                    Buttons();
                }
                
            }
        }
    }	

    public static void Buttons()
    {
        foreach(Transform t in I.buttonParent) Destroy(t.gameObject);

        foreach(int a in g.Games)
        {
            GameObject gg=Instantiate(I.buttonPrefab, I.buttonParent);
            gg.GetComponent<GameButton>().number = a;
            gg.GetComponent<GameButton>().Update();
        }
    }

    public static void LoadGame(int id)
    {
        gameId = id;
        Buttons();
    }

    public void SetPlayer(System.Int32 id)
    {
        playerId = FindObjectOfType<TMPro.TMP_Dropdown>().value;// (int)id;
        Debug.Log(FindObjectOfType<TMPro.TMP_Dropdown>().value);
    }

    void WebUpdate()
    {
        if(lastGet > Time.time - 0.03) return;

        lastGet = Time.time;
        if(g != null)
        {
            StartCoroutine(GetPlatforms());
            StartCoroutine(GetProjectiles());
            StartCoroutine(GetPlayers());
        }

        StartCoroutine(GetFirstGameID());
    }

    void Update()
    {


        UpdateState();
        WebUpdate();
    }

    void UpdateState()
    {
        if(m == null) 
		{	
			//Debug.Log("m is null");
			return;
		}
		if(p == null)
		{	
			//Debug.Log("p is null");
			return;
		}
		
        while(platforms.Count > m.Platforms.Length)
        {
            Destroy(platforms[platforms.Count-1].gameObject);
            platforms.Remove(platforms[platforms.Count - 1]);
        }
        while(platforms.Count < m.Platforms.Length)
        {
            platforms.Add(Instantiate(platfromPrefab).GetComponent<Platform>());
        }
        for(int i = 0; i < platforms.Count; i++)
        {
            platforms[i].Copy(m.Platforms[i]);
        }

	
	    while(players.Count > p.Players.Length)
	    {
	        Destroy(players[players.Count-1].gameObject);
	        players.Remove(players[players.Count - 1]);
	    }
	    while(players.Count < p.Players.Length)
	    {
	        players.Add(Instantiate(playerPrefab).GetComponent<Player>());
	    }
	    for(int i = 0; i < players.Count; i++)
	    {
	        players[i].Copy(p.Players[i]);
	    }

	    int ile = 0;
	    Player win = null;
	    foreach(Player p in players)
	    {
	        if(p.transform.position.y>-10)
	        {
	            ile++;
	            win = p;
	        }
	    }


	    if (ile==1)
	    {
	        vr.SetActive(true);
	        vr.GetComponent<TextMeshProUGUI>().text = "<size=25%>VICTORY ROYALE<size=100%>\n" + win.name;
	    }
	    else
	    {
	        vr.SetActive(false);
	    }
		

		if(o != null)
		{	
		    foreach (Projectile p in projectiles)
		    {
		        if (p.type == 0)
		        {
		            bool f = false;
		            foreach (ProjectileJSON pj in o.Projectiles)
		            {
		                Vector3 ppos = new Vector3(pj.X, pj.Y, pj.Z);
		                if ((ppos - p.transform.position).magnitude < 0.9f) f = true;
		            }
		            if (!f) Destroy(Instantiate(explosion, p.transform.position,Quaternion.identity), 3);
		        }
		    }

		    while (projectiles.Count > o.Projectiles.Length)
		    {
		        Destroy(projectiles[projectiles.Count - 1].gameObject);
		        projectiles.Remove(projectiles[projectiles.Count - 1]);
		    }
		    while (projectiles.Count < o.Projectiles.Length)
		    {
		        projectiles.Add(Instantiate(projectilePrefab).GetComponent<Projectile>());
		    }
		    for (int i = 0; i < projectiles.Count; i++)
		    {
		        projectiles[i].Copy(o.Projectiles[i]);
		    }
		}
    }

    void Awake()
    {
        I = this;
        StartCoroutine(GetNickMap());
    }


}
