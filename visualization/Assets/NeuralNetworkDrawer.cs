﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuralNetworkDrawer : MonoBehaviour
{
    public GameObject pyBall,pyLink;

    RenderTexture tex;

    public GameObject parent;
    List<GameObject>[] balls;

    public int[] minBalls;
    public int[] maxBalls;

    void BeCreative()
    {
        foreach(Transform t in parent.transform)
        {
            if (t.GetComponent<Camera>() == null)
                Destroy(t.gameObject);
        }

        float s = 0;
        float ss = 0;
        balls = new List<GameObject>[4];
        for(int layer =0;layer<4;layer++)
        {
            s = 0;
            balls[layer] = new List<GameObject>();

            int c = Random.Range(minBalls[layer], maxBalls[layer] + 1);

            float h = ss + Random.Range(-0.75f,-0.4f) * c;

            for(int i=0;i<c;i++)
            {
                balls[layer].Add(GameObject.Instantiate(pyBall, new Vector3(100 + layer * 4, h, 0), Quaternion.identity, parent.transform));
                balls[layer][i].GetComponent<MeshRenderer>().sharedMaterial = new Material(balls[layer][i].GetComponent<MeshRenderer>().sharedMaterial);
                balls[layer][i].GetComponent<MeshRenderer>().sharedMaterial.SetColor("_Color", new Color(Random.Range(0, 1f), 0, 0));
                s += h;
                h += Random.Range(0.7f, 1.5f);
            }

            ss = s / c;

            if(layer>0)
            {
                int C = c * balls[layer - 1].Count;

                C = Random.Range(C / 3, C * 3 / 4);
                for(int i=0;i<C;i++)
                {
                    GameObject a = GameObject.Instantiate(pyLink, parent.transform);
                    a.GetComponent<LineRenderer>().SetPosition(0, balls[layer - 1][Random.Range(0, balls[layer - 1].Count)].transform.position);
                    if (i < c) a.GetComponent<LineRenderer>().SetPosition(1, balls[layer][i].transform.position);
                    else a.GetComponent<LineRenderer>().SetPosition(1, balls[layer][Random.Range(0, balls[layer].Count)].transform.position);

                    a.GetComponent<LineRenderer>().sharedMaterial = new Material(a.GetComponent<LineRenderer>().sharedMaterial);
                    float ff = Random.Range(0, 1f);
                    a.GetComponent<LineRenderer>().sharedMaterial.SetColor("_Color", new Color(ff,0,ff));

                }
            }

            
        }
    }


    private void Update()
    {
        if (Random.Range(0, 100) == 0) BeCreative();
    }
    private void Awake()
    {
        BeCreative();
    }

    
}
