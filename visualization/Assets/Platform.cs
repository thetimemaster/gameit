﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Platform : MonoBehaviour
{

    bool hasStarted = false;

    public float radius;
    public void Copy(PlatformJSON json)
    {
        radius = json.Radius;
        transform.position = new Vector3(json.X,json.Y,json.Z);
        //transform.localScale = new Vector3(2 * radius, 0.001f, 2* radius);

        if(!hasStarted) 
        {
            hasStarted = true;
            GetComponent<MeshFilter>().sharedMesh = GenPlatformMesh.genMesh(1);
            GetComponent<LineRenderer>().positionCount = GetComponent<MeshFilter>().sharedMesh.vertices.Length;
            GetComponent<LineRenderer>().SetPositions(GetComponent<MeshFilter>().sharedMesh.vertices);
        }

        transform.localScale = Vector3.one * radius;
    }


}
