﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class Player : MonoBehaviour
{
    public static Player me;

    public string name;

    public Vector3 dp;

    public TMPro.TextMeshPro txt;

    public int id;

    public void Copy(PlayerJSON json)
    {

        Vector3 newV = new Vector3(json.X, json.Y, json.Z);
        if (newV != transform.position)
            dp = newV - transform.position;
        transform.position = newV;
        Vector3 v = new Vector3(json.VX, json.VY, json.VZ);
        if (dp.magnitude > 0)
        {
            Vector3 vv = new Vector3(json.VX, 0, json.VZ);
            vv.y = 0;
			if(vv != Vector3.zero)
            	transform.rotation = Quaternion.LookRotation(vv);
        }

        if(dp.magnitude > 0.7f)
        {
            GetComponentInChildren<TrailRenderer>().Clear();
        }

        if(NetHandler.nicks != null)
        {
            var a = NetHandler.nicks.Nicks.FirstOrDefault(el => el.user_id == json.ID);
            txt.text = a==null?"Player "+json.ID:NetHandler.DeXD(a.nick);
        }
        
        name = txt.text;
        if (json.ID == NetHandler.playerId) me = this;

        if(json.Y<-10)
        {
            foreach(Transform t in transform)
            {
                t.gameObject.SetActive(false);
            }
        }
        else
        {
            foreach (Transform t in transform)
            {
                t.gameObject.SetActive(true);
            }
        }
    }

    private void Update()
    {
        txt.transform.rotation = Quaternion.LookRotation(-(GameObject.Find("Main Camera").transform.position - transform.position));
    }
}
