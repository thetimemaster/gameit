﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public int type;

    public GameObject grenadeVis, holeVis;

    public void Copy(ProjectileJSON json)
    {
        Vector3 v = new Vector3(json.X, json.Y, json.Z);
        transform.LookAt(v);
        transform.position = v;
        type = json.Type;


        grenadeVis.SetActive(type == 0);
        holeVis.SetActive(type != 0);
    }
}
